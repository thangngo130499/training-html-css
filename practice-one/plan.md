# PRACTICE ONE

## NOTES

In this practice, supporters should help trainees know how to slice layouts/components from design. Besides, supporters should choose a design like 1-2 pages that have a base HTML structure. Not too fussy or have special drawings.

## OVERVIEW
- 1 developer (Thang Ngo. Mentor: Khanh Nguyen)
- Design: [odigo](https://www.figma.com/file/GEdIowP8MaUWaMRaArKe4W/travel-landing-page-jacobvoyles?node-id=0%3A1)
- Timeline: 5 days (20/01/2022)

## REQUIREMENT

- Work fine on Chrome browser latest version
- Use the right HTML Tags
- Apply Flexbox and Grid
- Use validate tool: [W3 Validator](https://validator.w3.org/)

## TARGET

- Build a website with HTML/CSS from the design website
- Familiar with common HTML tags and CSS selectors
- Understand the concepts of Flex and Grid

## TECHNICAL STACK

### HTML5

- Hypertext Markup Language revision 5 (HTML5) is markup language for the structure and presentation of World Wide Web contents. HTML5 supports the traditional HTML and XHTML-style syntax and other new features in its markup, New APIs, XHTML and error handling.

### CSS3

- A cascading style sheet (CSS) is a Web page derived from multiple sources with a defined order of precedence where the definitions of any style element conflict.

## TOOL

- VS Code

## PLAN

- Implement code skeleton (3 hours)
- Implement header (1 day)
    - [x] Implement navigation
    - [x] Implement text header
    - [x] Implement text support
    - [x] Implement search-1
    - [x] Implement search-2
    - [x] Implement search-btn
    - [x] Implement background header
- Implement benefit section (1 day)
    - [x] Implement text header
    - [x] Implement box-1
    - [x] Implement box-2
    - [x] Implement box-3
    - [x] Implement button view learn more
- Implement get inspired section (1 day)
    - [x] Implement text header
    - [x] Implement button view all
    - [x] Implement title and photo composition
- Implement Prefecture & Neighborhood section (1 day)
    - [x] Implement text header
    - [X] Implement title and photo composition
    - [x] Implement button view prefecture
- Implement today top places section (1 day)
    - [x] Implement text header
    - [x] Implement title and photo composition
    - [x] Implement button view see more
- Implement video (4 hours)
    - [x] Implement video
- Implement footer (4 hours)
    - [x] Implement logo
    - [x] Implement link-1
    - [x] Implement link-2
    - [x] Implement link-3